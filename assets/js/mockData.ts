import { sub } from 'date-fns';

function getRandomArbitrary (min: number, max: number): number {
  return Math.random() * (max - min) + min;
}

// Create time series for 11 years = 44 quarters
export const dividendTimeSeries =
  [...Array(44).keys()]
    .map((_: number, index: number) => ({
      value: getRandomArbitrary(2, 6),
      yield: getRandomArbitrary(2.2, 3.5),
      date: sub(new Date(2021, 11, 31, 6, 0, 0), { months: index * 3 })
    }))
    .reverse();
