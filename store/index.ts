import axios from 'axios';

const SET_FEED_DATA = 'SET_FEED_DATA';

export const state = () => ({
  dividendCalendar: [{
    companyName: 'Bayer',
    date: '2021-11-28',
    dividendAmount: 2.38,
    dividendCurrency: 'EUR'
  }],
  earningsReports: [{
    companyName: 'Bayer',
    date: '2021-11-28',
    reportLink: 'https://www.bayer.com/sites/default/files/2021-05/bayer-quartalsmitteilung-q1-2021.pdf'
  }],
  feed: {
    alleaktien: '',
    finfo: ''
  },
  flops: [{
    currency: 'EUR',
    differenceInPercent: -4.11,
    name: 'Covestro',
    price: 53.2,
    timestamp: '2021-11-07T04:00:00.000Z'
  }, {
    currency: 'EUR',
    differenceInPercent: -2.08,
    name: 'Daimler',
    price: 72.1,
    timestamp: '2021-11-07T04:00:00.000Z'
  }],
  highlight: {
    linkUrl: 'https://www.finanzen.net/nachricht/aktien/corona-folgen-varta-aktie-letztendlich-im-freien-fall-varta-senkt-umsatzprognose-wegen-produktionsausfaellen-bei-kunden-10710622',
    text: 'Gewinnwarnung bei Varta. Rücksetzer als Kaufchance?'
  },
  hotStocks: [{
    name: 'Apple',
    clicks: 12345,
    differenceInPercent: 12.49
  }, {
    name: 'Siemens Energy',
    clicks: 5873,
    differenceInPercent: -5.49
  }],
  insiderTrades: [{
    amount: 23500.87,
    companyName: 'Baader Bank',
    currency: 'EUR',
    timestamp: '2021-11-07T04:00:00.000Z',
    traderName: 'Oliver Riedel',
    traderRole: 'Vorstand',
    tradeType: 'BUY'
  }],
  marketData: [{
    differenceInPercent: 0.23,
    indexName: 'Dow Jones',
    marketId: 1,
    totalValue: 36342.34,
    historicalData: [
      // TODO add historical data
    ]
  }, {
    differenceInPercent: -0.05,
    indexName: 'DAX',
    marketId: 2,
    totalValue: 16003.01,
    historicalData: [
      // TODO add historical data
    ]
  }],
  news: [{
    id: 1,
    imageUrl: 'https://images.finanzen.net/mediacenter/unsortiert/tesla-ken-wolter-660-3834_w305.jpg',
    linkurl: 'https://www.finanzen.net/nachricht/aktien/twitter-umfrage-tesla-aktie-knickt-vorboerslich-ein-elon-musk-laesst-sich-von-twitter-nutzern-zu-aktienverkauf-in-milliardenhoehe-verpflichten-10716320',
    timestamp: '2021-11-07T04:00:00.000Z',
    title: 'Tesla-Aktie knickt vorbörslich ein: Elon Musk lässt sich von Twitter-Nutzern zu Aktienverkauf in Milliardenhöhe verpflichten'
  }, {
    id: 2,
    imageUrl: 'https://images.finanzen.net/mediacenter/unsortiert/amazon-sundry-photography-shutterstock-76222-660_w305.jpg',
    linkurl: 'https://www.finanzen.net/nachricht/aktien/e-autobauer-vor-boersengang-kurz-vor-rivian-ipo-diesen-anteil-haelt-amazon-an-dem-tesla-konkurrenten-10700110',
    timestamp: '2021-11-05T02:43:00.000Z',
    title: 'Kurz vor Rivian-IPO: Diesen Anteil hält Amazon an dem Tesla-Konkurrenten'
  }, {
    id: 3,
    imageUrl: 'https://images.finanzen.net/mediacenter/unsortiert/disney-chrisdorney-660-7017_w188.jpg',
    linkurl: 'https://www.finanzen.net/nachricht/aktien/unter-erwartungen-disney-film-34-eternals-34-spielt-am-ersten-wochenende-weniger-ein-als-gedacht-disney-aktie-vorboerslich-fester-10716053',
    timestamp: '2021-11-03T04:08:00.000Z',
    title: 'Disney-Film "Eternals" spielt am ersten Wochenende weniger ein als gedacht - Disney-Aktie vorbörslich fester'
  }, {
    id: 4,
    imageUrl: 'https://images.finanzen.net/mediacenter/unsortiert/ethereum-shutterstock-658494145-by-lightboxx-660_w188.jpg',
    linkurl: 'https://www.finanzen.net/nachricht/devisen/im-rallymodus-bitcoin-naehert-sich-rekordhoch-ether-so-teuer-wie-nie-10717545',
    timestamp: '2021-11-05T04:12:00.000Z',
    title: 'Bitcoin nähert sich Rekordhoch - Ether so teuer wie nie'
  }],
  tops: [{
    currency: 'USD',
    differenceInPercent: 2.34,
    name: 'Apple',
    price: 243.89,
    timestamp: '2021-11-07T04:00:00.000Z'
  },
  {
    currency: 'USD',
    differenceInPercent: 5.12,
    name: 'Microsoft',
    price: 335.07,
    timestamp: '2021-10-05T04:00:00.000Z'
  }]
});

export const actions = {
  async nuxtServerInit ({ commit }: any, _: any) {
    try {
      const { data } = await axios.get('https://www.alleaktien.de/feed/');
      commit(SET_FEED_DATA, { alleaktien: data });
    } catch (ex) {
      console.error('Could not retrieve AlleAktien feed', ex);
    }

    try {
      const { data } = await axios.get('https://us12.campaign-archive.com/home/?u=17990761a9cf4f4ba7aa4d7da&id=7767ea2372');
      commit(SET_FEED_DATA, { finfo: data });
    } catch (ex) {
      console.error('Could not retrieve AlleAktien feed', ex);
    }
  }
};

export const mutations = {
  [SET_FEED_DATA]: (state: any, data: any) => {
    state.feed = {
      ...state.feed,
      ...data
    };
  }
};
