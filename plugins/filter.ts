import Vue from 'vue';
import { format } from 'date-fns';

Vue.filter('currency', (amount: number, currency: string) => {
  return `${amount} ${currency}`;
});

Vue.filter('date', (value: string, dateFormat = 'dd.MM.yyyy') => {
  return format(new Date(value), dateFormat);
});

Vue.filter('tradeType', (type: string) => {
  switch (type) {
    case 'BUY':
      return 'Kauf';
    case 'SELL':
      return 'Verkauf';
    default:
      return 'Andere';
  }
});
