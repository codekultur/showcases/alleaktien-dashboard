export interface Article {
  id: number;
  imageUrl: string;
  linkurl: string;
  timestamp: string;
  title: string;
};

export interface Stock {
  currency: string;
  differenceInPercent: number;
  name: string;
  price: number;
  timestamp: string;
};
