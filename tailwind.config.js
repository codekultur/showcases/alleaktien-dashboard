module.exports = {
  darkMode: 'class',
  plugins: [],
  purge: [
    './components/**/*.{vue,js}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}'
  ],
  theme: {
    flex: {
      1: '1 1 25%',
      2: '1 1 50%',
      auto: '1 1 auto',
      initial: '0 1 auto',
      none: 'none'
    },
    gridTemplateColumns: {
      topFlopItem: '1fr 1fr 4rem'
    }
  },
  variants: {
    extend: {}
  }
};
